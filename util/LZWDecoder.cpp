//
// Created by martin on 20.11.17.
//

#include <iostream>
#include "LZWDecoder.h"

LZWDecoder::LZWDecoder(std::string path) {
    std::vector<char> file = readFile(path);
    initDictionaryWithAscii();
    getCodesFromFile(file);
}

LZWDecoder::~LZWDecoder() {

}


bool LZWDecoder::isOddNumberOfCodes(std::vector<char> file) {
    return file.size() % 3 != 0;
}


std::vector<char> LZWDecoder::readFile(std::string path) {

    std::ifstream inputFile (path, std::ios::binary|std::ios::ate);

    if(inputFile){
        std::ifstream::pos_type pos = inputFile.tellg();

        std::vector<char> result(pos);

        inputFile.seekg(0, std::ios::beg);
        inputFile.read(&result[0], pos);
        inputFile.close();
        return result;
    }

    else {
        std::cout<<"Count not find file!"<<std::endl;
        exit (EXIT_FAILURE);
    }
}

void LZWDecoder::initDictionaryWithAscii() {

    for (int i = 0; i < 256; i++) {
        mDictionary[i] = (char)i;
    }
}

void LZWDecoder::getCodesFromFile(std::vector<char> file) {
    //Twelve bit fixed size: 00000100|1000 00011|1110100 => Three bytes get combined to two numbers

    //Size of char in C++ = 1 byte
    unsigned char byte1, byte2, byte3;
    int firstNumber, secondNumber;

    int limiter = (int)(isOddNumberOfCodes(file) ? (file.size() - 2) : (file.size()));

    for(int i = 0; i < limiter; i=i+3){
        byte1 = file.at(i);
        byte2 = file.at(i+1);
        byte3 = file.at(i+2);

        firstNumber = (byte1 << 4) + (byte2 >> 4);
        secondNumber = ((byte2 & 0b00001111) << 8) + byte3;

        mCodes.push_back(firstNumber);
        mCodes.push_back(secondNumber);
    }

    if(isOddNumberOfCodes(file)){
        byte1 = file.at(limiter);
        byte2 = file.at(limiter+1);
        int number = byte1 << 8 + byte2;
        mCodes.push_back(number);
    }
}

void LZWDecoder::decodeFile() {
    std::string result;
    std::string lastOutput;
    std::string thisOutput;

    lastOutput = mDictionary.at(mCodes.at(0));
    result.append(lastOutput);

    for(int i = 1; i < mCodes.size(); i++){
        int temp = mCodes.at(i);

        if (mDictionary.find(temp) == mDictionary.end()){
            thisOutput = lastOutput + lastOutput.at(0);
        } else {
            thisOutput = mDictionary.at(temp);
        }

        pushToDictionary(lastOutput+thisOutput.at(0));
        result.append(thisOutput);

        lastOutput = thisOutput;
    }

    std::cout<<result<<std::endl;
}

void LZWDecoder::pushToDictionary(std::string val) {
    if(mDictionary.size() == 4096){
        mDictionary.clear();
        initDictionaryWithAscii();
    }
    mDictionary.insert(std::pair<int,std::string>(mDictionary.size(),val));
}


