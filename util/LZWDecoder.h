/**
 * LZW Decoder created for StarLeaf in order to convince the company of my skills and in order to get the chance to do
 * an internship.
 *
 * LZW explanation: https://www.youtube.com/watch?v=mxqD315rYnA and
 *                  https://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Welch
 *
 * @author Martin Schneglberger
 */

#ifndef STARLEAFLZW_READFILE_H
#define STARLEAFLZW_READFILE_H


#include <fstream>
#include <map>
#include <vector>

class LZWDecoder {

private:
    std::map<int,std::string> mDictionary;
    std::vector<int> mCodes;

    std::vector<char> readFile(std::string path);
    bool isOddNumberOfCodes(std::vector<char> file);
    void initDictionaryWithAscii();
    void getCodesFromFile(std::vector<char> vector);
    void pushToDictionary(std::string val);

public:

    LZWDecoder(std::string path);
    ~LZWDecoder();

    void decodeFile();
};


#endif //STARLEAFLZW_READFILE_H
